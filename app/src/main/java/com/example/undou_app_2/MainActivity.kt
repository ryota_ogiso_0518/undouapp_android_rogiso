package com.example.undou_app_2

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CalendarView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.memo_view.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        var text = "go"
        val calendarView = findViewById<CalendarView>(R.id.calendar)
        val format = SimpleDateFormat("yyyy/MM/dd", Locale.US)

        val date = calendarView.date
        val dateSt = format.format(date)
        //text = intent.getStringExtra("TEXT_KEY1")
       // textView11.text = dateSt
        if(text == "go"){
            textView20.setBackgroundColor(Color.rgb(4,243,223))
            textView11.text = "今日は運動しました!!明日も頑張りましょう!!"
        }

        // 日付変更イベントを追加
        calendarView.setOnDateChangeListener { view, year, month, dayOfMonth ->
            val date = "$year/$month/$dayOfMonth"
            val intent = Intent(this@MainActivity, MemoActivity::class.java)
            intent.putExtra("TEXT_KEY",dateSt)
            startActivity(intent)
            Toast.makeText(this, date, Toast.LENGTH_SHORT).show()
        }
    }
}
