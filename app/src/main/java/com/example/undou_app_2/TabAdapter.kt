package com.example.undou_app_2

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.fragment.app.FragmentStatePagerAdapter
import com.google.android.material.tabs.TabLayout

class TabAdapter(fm: FragmentManager, private val context: Context) : FragmentStatePagerAdapter(fm) {
    private val pageTitle = arrayOf("PAGE1", "PAGE2", "PAGE3")
    //
    override fun getItem(position: Int): Fragment {
        // 要求時 新しい Fragment を生成して返す
        return TabFragments.newInstance(position + 1)
    }
    // タブの名前
    override fun getPageTitle(position: Int): CharSequence? {
        return pageTitle[position]
    }
    // タブの個数
    override fun getCount(): Int {
        return pageTitle.size
    }
    // タブの変更
    fun getTabView(tabLayout: TabLayout, position: Int): View {
        // tab_item.xml を複数
        val view = LayoutInflater.from(this.context).inflate(R.layout.tab_item, tabLayout, false)
        // タイトル
        val tab = view.findViewById<TextView>(R.id.teb_item_text)
        tab.text = pageTitle[position]
        // アイコン
        val image = view.findViewById<ImageView>(R.id.teb_item_image)
        image.setImageResource(R.mipmap.ic_launcher)
        return view
    }
}