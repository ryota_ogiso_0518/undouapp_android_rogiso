package com.example.undou_app_2
import java.io.File
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.memo_view.*
import android.widget.Toast
import org.w3c.dom.Text


class memoFragment1 : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.memo_view, container, false)

    }
//    companion object{
//        fun newInstance():memoFragment1{
//            return memoFragment1()
//        }
//    }

    override fun onStart() {
        super.onStart()

        val txt = activity!!.findViewById(R.id.textView8)as TextView
        val spinner = activity!!.findViewById(R.id.spinner)as Spinner

        val button2 = activity!!.findViewById(R.id.button3)as Button
        var str = ""
        var num = ""
        var num1 = 0
        var cal = 0
        var scal = ""

        button2.setOnClickListener {
            str = spinner.selectedItem as String
            num = editText.text.toString()
            num1 = num.toInt()

            if(str == "ランニング(中)"){
                cal = (1.05 * 7 * num1).toInt()
                textView18.text = "20"
            }else if(str == "ウォーキング(弱)"){
                cal = (1.05 * 4 * num1).toInt()
                textView18.text = "20"
            }else if(str == "水泳(強)"){
                cal = (1.05 * 8 * num1).toInt()
                textView18.text = "20"
            }else if(str == "スポーツ(強)"){
                cal = (1.05 * 7 * num1).toInt()
                textView18.text = "20"
            }
            scal = cal.toString()
            textView2.text = scal

            if(cal >= 20){
                textView21.text = "よくできました!!"
            }else{
                textView21.text = "頑張りましょう!!"
            }
        }

    }

}
