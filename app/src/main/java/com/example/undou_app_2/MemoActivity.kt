package com.example.undou_app_2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.example.undou_app_2.R
import com.example.undou_app_2.memoAdapter
import com.example.undou_app_2.memoFragment1
import com.example.undou_app_2.memoFragment2
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_memo.*
import kotlinx.android.synthetic.main.memo_view.*
import android.content.Intent

class MemoActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_memo)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        var und = ""
        var kin = ""
        //var fm = memoFragment1.newInstance()
        val ft = supportFragmentManager.beginTransaction()
        //ft.add(R.id.MyConst,fm,"0")
        ft.commit()

        val text = intent.getStringExtra("TEXT_KEY") + "の運動報告"
        /// フラグメントのリストを作成
        val fragmentList = arrayListOf<Fragment>(
            memoFragment1(),
            memoFragment2()
        )
        button4.setOnClickListener {
            val intent = Intent(this@MemoActivity, MainActivity::class.java)
            und = "go"
            intent.putExtra("TEXT_KEY1",und)
            intent.putExtra("TEXT_KEY2",kin)
            startActivity(intent)
        }
        supportActionBar?.title = text
        /// adapterのインスタンス生成
        val adapter = memoAdapter(supportFragmentManager, fragmentList)
        /// adapterをセット
        viewPager.adapter = adapter
    }
    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }
    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home->{
                finish()
            }
        }
        return super.onOptionsItemSelected(item)
    }

}