import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.undou_app_2.R

class TabFragments : Fragment() {
    //呼び出しもとのアクティビティのインスタンス
    private var mListener : OnFragmentInteractionListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    //
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val page = arguments!!.getInt(PAGE, 0)
        // fragment_pageを複製しtextへページを表示
        val view = inflater.inflate(R.layout.flagment_page, container, false)
        view.findViewById<TextView>(R.id.textView).text = page.toString()
        return view
    }
    // 親アクティビティとの紐づけ発生イベント
    override fun onAttach(context: Context) {
        if (context != null) {
            super.onAttach(context)
        }
        if (context is OnFragmentInteractionListener) {
            mListener = context
        } else {
            throw RuntimeException()
        }
    }
    override fun onDetach() {
        super.onDetach()
        mListener = null
    }
    internal interface OnFragmentInteractionListener {
        fun onFragmentInteraction(uri: Uri)
    }
    companion object {
        private val PAGE = "PAGE"
        // PageFragment生成
        fun newInstance(page: Int): TabFragments {
            val pageFragment = TabFragments()
            val bundle = Bundle()
            bundle.putInt(PAGE, page)
            pageFragment.arguments = bundle
            return pageFragment
        }
    }
}