package com.example.undou_app_2

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Spinner
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.memo_view.*
import kotlinx.android.synthetic.main.memo_view_2.*

class memoFragment2 : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.memo_view_2, container, false)
    }

    override fun onStart() {
        super.onStart()
        val button = activity!!.findViewById(R.id.button)as Button
        val spinner = activity!!.findViewById(R.id.spinner2)as Spinner
        var str = ""
        var num = ""
        var num1 = 0
        button.setOnClickListener {
            str = spinner.selectedItem as String
            textView22.text = str +" " + editText4.text + "回"
            num = editText4.text.toString()
            num1 = num.toInt()
            if(num1 >= 30){
                textView6.text = "よくできました!!"
            }else{
                textView6.text = "もう少し頑張ろう!!"
            }
        }

    }
}